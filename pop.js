// Using the array function which pops the last element of an array, 
// remove the last element from the values array and store it in a new variable called bool.

let values = ["Jason", 4, true];
let bool = values.pop();
console.log(bool);