// Using a single JavaScript statement, replace the value of the 
// fourth element in the array with a new numeric value so that all elements are in ascending order.

let list = [1, 2, 3, 7, 5, 6, 7, 8, 9];
list[3] = 4;
console.log(list);


// Create a new array called person, containing the following value types in this exact order:
// First element should be a String.
// Second element should be a Number
// Third element should be a Boolean

let person = ["Zahid",24 ,true];
console.log(person);