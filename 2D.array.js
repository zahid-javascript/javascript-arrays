// Arrays can hold lots of different types of data. Strings, 
// values, variables, other arrays, and a combination of them all. 
// Let's practice working with some of these more advanced arrays.

let movie1 = [16, "Candles"];
let movie2 = [3, "Men", "and", "a", "Baby"];
let eightiesMovies = [movie1, movie2];

// Using the existing eightiesMovies array, find a way to extract the word "Baby". 
// Assign it to a new variable called infant and use 
// console.log to output this value to the console.

let infant = eightiesMovies[1][4];
console.log(infant);

// Now let's add a console.log that will output a string with the full title 
// of the movie assigned to movie1 in the eightiesMovies array.
// Do that by using just the eightiesMovies variable.
// Note: the movie title in question is 16 Candles.

console.log(`${eightiesMovies[0][0]} ${eightiesMovies[0][1]}`);