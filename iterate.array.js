// Let's finish writing the numStrings function.
// Inside this function we will use a for loop that counts 
// all of the strings in the array parameter called list.

// Inside the numStrings function, create a count variable and assign it a value of 0. 
// We'll use this variable to keep track of the number of strings.

// Now using a for loop, iterate through list one element at a time. 
// The loop should declare a new variable i and set its initial value to 0. 
// Run the loop while i is less than list.length. After each iteration, increase i by 1.

// Inside the numStrings function, create a count variable and assign it a value of 0. 
// We'll use this variable to keep track of the number of strings.

// Now using a for loop, iterate through list one element at a time. 
// The loop should declare a new variable i and set its initial value to 0. 
// Run the loop while i is less than list.length. After each iteration, increase i by 1.
// For this next step, we'll make use of the previously defined isString function. 
// Inside this function, we are using the typeof operator which we haven't talked about yet. 
// The typeof operator returns a string value describing its type (like "string", "number", "boolean", etc.).
// Inside the for loop, create an if statement that calls isString with the current element of the loop.
// If the return from this function call is true, then increment count by 1.

let list = ['1', 2, 3, '7', 5, 6, '7', 8, '9'];
let countOfStrings = numStrings(list);
console.log(countOfStrings);

function isString(value){
    return typeof value == "string";
}
  
function numStrings(list) {
    let count = 0;
    for(var i = 0; i < list.length; i++){
        if(isString(list[i])){
            count += 1;
        }
    }
    return count;
}


  