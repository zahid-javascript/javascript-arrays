// Using the function that adds data to the end of an array, 
// add the number 10 to our list array.

let list = [1, 2, 3, 4, 5, 6, 7, 8, 9];
list.push(10);
console.log(list);